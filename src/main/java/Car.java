/**
 * Created by Triven on 21.04.2016.
 */
public class Car extends Thread {
    private String name;
    private Sound sound;

    public Car(Sound sound, String name) {
        this.sound = sound;
        this.name = name;
    }

    @Override
    public void run() {

        try {
            sound.beep();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(name);
    }
}
