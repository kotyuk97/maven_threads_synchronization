import java.awt.*;

/**
 * Created by Triven on 21.04.2016.
 */
public class Sound {

    public synchronized void beep() throws InterruptedException {
        Toolkit.getDefaultToolkit().beep();
        Thread.sleep(2000);
    }

    public synchronized void beep(int i) throws InterruptedException {
        for (int tmp = 0; tmp < i; tmp++) {
            Toolkit.getDefaultToolkit().beep();
            Thread.sleep(100);
        }
    }
}
