/**
 * Created by Triven on 21.04.2016.
 */
public class Main {

    public static void main(String... args) throws InterruptedException {
        System.out.println("Hallo Leute!!");
        Sound s = new Sound();
        Car audi = new Car(s, "Audi");
        Car bmw = new Car(s, "BMW");
        Car smart = new Car(s, "Smart");

        smart.start();
        bmw.start();
        audi.start();

        Thread.sleep(5000);

        smart.stop();
        bmw.stop();
        audi.stop();

        System.out.println("end");

    }
}
